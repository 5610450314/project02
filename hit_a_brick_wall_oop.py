import sys, os, math, random
import pygame
from pygame.locals import *


class Paddle(pygame.sprite.Sprite):
    def __init__(self, xy): 
        pygame.sprite.Sprite.__init__(self)     
        self.image = pygame.image.load(os.path.join('images','paddle_normal.png'))
        self.rect = self.image.get_rect()
        self.rect.centerx, self.rect.centery = xy

        self.movementspeed = 7     #speed paddle
        self.velocity = 0

    def left(self):
        self.velocity -= self.movementspeed

    def right(self):
        self.velocity += self.movementspeed

    def move(self, dx):
        if dx != 0:
            if self.rect.right + dx > 800:  #limit of paddle can move in the x dir.
                self.rect.right = 799
            elif self.rect.left + dx < 0:
                self.rect.left = 1
            else:
                self.rect.x += dx

    def update(self):
        self.move(self.velocity)

    def reset(self):
        self.rect.center = 400, 670 
        
    def grow(self):
        xy = self.rect.center    
        self.image = pygame.image.load(os.path.join('images','paddle_long.png')) 
        self.rect = self.image.get_rect()  
        self.rect.centerx, self.rect.centery = xy 

        if self.rect.right > 800:
            self.rect.right =799
        elif self.rect.left < 0:
            self.rect.left = 1

    def shrink(self):                                #Returns size
        xy = self.rect.center   
        self.image = pygame.image.load(os.path.join('images','paddle_normal.png'))
        self.rect = self.image.get_rect()     
        self.rect.centerx, self.rect.centery = xy     

        if self.rect.right > 800:  
            self.rect.right = 799
        elif self.rect.left < 0:
            self.rect.left = 1


class Ball(pygame.sprite.Sprite):
    def __init__(self, xy):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join('images','ball_blue.png'))
        self.rect = self.image.get_rect()
        self.rect.centerx, self.rect.centery = xy

        self.maxspeed = 9    
        self.servespeed = 5
        self.damage = 1

        self.velx = 0   
        self.vely = 0

    def move(self, dx, dy):
        self.rect.x += dx          #Move ball
        self.rect.y += dy

    def update(self):
        speed = math.hypot(self.velx, self.vely)
        if speed > self.maxspeed:
            speed = self.maxspeed                       
            angle = math.atan2(self.vely, self.velx)    # get angle
            self.velx = math.cos(angle) * speed
            self.vely = math.sin(angle) * speed

        self.move(self.velx, self.vely)   # move ball

    def reset(self):
        self.rect.centerx = 350
        self.rect.bottom = 665
        self.velx = 0
        self.vely = 0

    def serve(self):
        angle = -90 + random.randint(-30, 30)
        x = math.cos(math.radians(angle))
        y = math.sin(math.radians(angle))

        self.velx = self.servespeed * x
        self.vely = self.servespeed * y

    def slowDown(self):
        self.maxspeed = 5

    def speedUp(self):
        self.maxspeed = 9


class Block(pygame.sprite.Sprite):
    def __init__(self, xy, images, level=1):
        pygame.sprite.Sprite.__init__(self)
        self.images = images 
        self.level = level
        self.image = self.images[self.level]
        self.rect = self.image.get_rect()
        self.rect.center = xy

    def hit(self, damage=1):
        points = 0
        while damage > 0 and self.level > 0:
            if self.level == 1:   #Ice brick 1 has 100 point/ice-brick set
                points += 100 * (self.level/self.level)
                self.level -= damage
                damage -= 1
            elif self.level > 3:   #colors brick 1 have 50 point/brick
                points += 50 * (self.level/self.level)    
                self.level -= damage
                damage -= 1
            else:
                points += 0            #Ice bick 3 & 2 no point
                self.level -= damage
                damage -= 1                
                
            
            
            
        if self.level < 3 and self.level !=0:    #Ice brick 3 -> 2 -> 1
            self.image = self.images[self.level]
            xy = self.rect.center               
            self.rect = self.image.get_rect()
            self.rect.center = xy              
            return False, points
        else:
            return True, points     #Color brick   
             


class SolidBlock(pygame.sprite.Sprite):  #can't destroyed
    def __init__(self, xy):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join('images', 'brick_hin.png'))
        self.rect = self.image.get_rect()
        self.rect.center = xy 

    def hit(self, damage):
        return False, 0    #Returns false, 0 since it cannot be destroyed


class BlockFactory():
    def __init__(self):
        # load all block images
        self.images = {
            1: pygame.image.load(os.path.join('images','brick_ice3.png')),
            2: pygame.image.load(os.path.join('images','brick_ice2.png')),
            3: pygame.image.load(os.path.join('images','brick_ice1.png')),            
            4: pygame.image.load(os.path.join('images','brick_1.png')),
            5: pygame.image.load(os.path.join('images','brick_2.png')),
            6: pygame.image.load(os.path.join('images','brick_3.png')),
            7: pygame.image.load(os.path.join('images','brick_4.png')),
            8: pygame.image.load(os.path.join('images','brick_5.png')),
            9: pygame.image.load(os.path.join('images','brick_6.png')),
            10: pygame.image.load(os.path.join('images','brick_7.png')),
            11: pygame.image.load(os.path.join('images','brick_8.png')),
            12: pygame.image.load(os.path.join('images','brick_9.png')),
            13: pygame.image.load(os.path.join('images','brick_10.png')),
            14: pygame.image.load(os.path.join('images','brick_11.png')),
            15: pygame.image.load(os.path.join('images','brick_12.png')),
            16: pygame.image.load(os.path.join('images','brick_13.png')),
            17: pygame.image.load(os.path.join('images','brick_14.png')),
            18: pygame.image.load(os.path.join('images','brick_15.png')),
            19: pygame.image.load(os.path.join('images','brick_16.png')),
            20: pygame.image.load(os.path.join('images','brick_17.png')),
            21: pygame.image.load(os.path.join('images','brick_18.png')),
            22: pygame.image.load(os.path.join('images','brick_19.png')),
            23: pygame.image.load(os.path.join('images','brick_20.png')),
            24: pygame.image.load(os.path.join('images','brick_21.png')),
            25: pygame.image.load(os.path.join('images','brick_22.png')),
        }
        

    def getBlock(self, xy, level=1):
        return Block(xy, self.images, level)

    def getSolidBlock(self, xy):
        return SolidBlock(xy)


class Score(pygame.sprite.Sprite):
    def __init__(self, xy):
        pygame.sprite.Sprite.__init__(self)
        self.xy = xy
        self.font = pygame.font.Font("5012_tLU_infection.ttf",50)
        self.color = (80,150,156)
        self.score = 0  # start at zero
        self.reRender()

    def update(self):
        pass

    def add(self, points):
        self.score += points
        self.reRender()

    def reset(self):
        self.score = 0    #Resets scores to zero
        self.reRender()

    def reRender(self):
        self.image = self.font.render("Score : %d"%(self.score), True, self.color)
        self.rect = self.image.get_rect()
        self.rect.center = self.xy


class ShowLevel(pygame.sprite.Sprite):
    def __init__(self, xy):
        pygame.sprite.Sprite.__init__(self)
        self.xy = xy
        self.font = pygame.font.Font("5012_tLU_infection.ttf",39)
        self.color = (80,150,156)
        self.level = 1  # start at 1st level
        self.reRender()

    def update(self):
        pass

    def add(self):
        self.level += 1
        self.reRender()

    def reset(self):
        self.level = 1    #Resets starting at 1st level
        self.reRender()

    def reRender(self):
        self.image = self.font.render("Level : %d"%(self.level), True, self.color)
        self.rect = self.image.get_rect()
        self.rect.center = self.xy




class Lives(pygame.sprite.Sprite):
    def __init__(self, xy, startinglives=5):
        pygame.sprite.Sprite.__init__(self)
        self.xy = xy 
        self.heartimage = pygame.image.load(os.path.join('images','PicHeart.png'))
        self.setLives(startinglives)

    def getLives(self):
        return self.lives

    def setLives(self, lives):
        self.lives = lives
        self.generateLivesImage()

    def generateLivesImage(self):
        heartrect = self.heartimage.get_rect()
        heart = 5
        newwidth = (heartrect.width + heart) * self.lives
        
        surface = pygame.Surface( (newwidth, heartrect.height) )
        
        for l in range(self.lives):
            surface.blit(self.heartimage, ((heartrect.width + heart) * l, 0))

        self.image = surface
        self.rect = surface.get_rect()
        self.rect.centerx, self.rect.centery = self.xy


class Powerup(pygame.sprite.Sprite):
    def __init__(self, type='bigpaddle'):
        pygame.sprite.Sprite.__init__(self)

        self.type = type
        self.collected = False
        self.countdown = 1

        if type == 'bigpaddle':
            self.countdown = 60 * 25
        elif type == 'slowball':
            self.countdown = 60 * 10

        self.imagepaths = {
            'bigpaddle': os.path.join('images', 'random_ball.png'),
            '1up': os.path.join('images', 'random_ball.png'),
            'slowball': os.path.join('images', 'random_ball.png'),
        }

        self.image = pygame.image.load(self.imagepaths[type])  #set image & rect
        self.rect = self.image.get_rect()

        self.rect.center = random.randint(20, 500), 125

    def update(self):
        self.rect.y += 2
        if self.rect.y > 670:
            return False
        return True
    


class Game():
    def __init__(self):

        pygame.init()
        self.window = pygame.display.set_mode((800, 700))
        self.clock = pygame.time.Clock()
        pygame.display.set_caption("Project02 - Hit a brick Wall")

        self.background = pygame.image.load(os.path.join('images','BG_it.png'))
        self.window.blit(self.background, (0,0))  # blit background onto window
        pygame.display.flip()
        
        pygame.mixer.pre_init(48000, -16, 1,256)        
        pygame.mixer.init()
        self.end_sound = pygame.mixer.Sound(os.path.join('sounds','theEnd.wav'))
        self.win_sound = pygame.mixer.Sound(os.path.join('sounds','TaDa.wav'))            
        
        pygame.mixer.music.load(os.path.join('sounds','Future_World.wav'))
        pygame.mixer.music.play(-1)        
        
        self.sprites = pygame.sprite.RenderUpdates()

        self.paddle = Paddle((400,670))         #paddle
        self.sprites.add(self.paddle)

        self.ball = Ball((0,0))      #ball
        self.ball.reset()
        self.sprites.add(self.ball)

        self.blocks = pygame.sprite.RenderUpdates()

        self.blockfactory = BlockFactory()

        self.currentlevel = 1        #load 1st level
        self.loadLevel(self.currentlevel)

        self.isReset = True
        self.playing = True

        self.score = Score((650, 30))      #score
        self.sprites.add(self.score)
        
        self.level = ShowLevel((400, 30))    #level
        self.sprites.add(self.level)        

        self.lives = Lives((120, 30), 5)      #lives
        self.sprites.add(self.lives)

        self.currentpowerup = None 
        self.powerupdrop = 60 * 10
   


    def run(self):

        running = True
        while running:

            self.clock.tick(60)
            running = self.handleEvents()
                
            if self.playing:
                
                for sprite in self.sprites:        # update our sprites
                    alive = sprite.update()
                    if alive is False:
                        self.sprites.remove(sprite)

                self.manageBall()
                self.manageCollisions()
                if len(self.blocks) == 0:
                    self.newLevel()
                self.managePowerups()
                self.sprites.clear(self.window, self.background)
                dirty = self.sprites.draw(self.window)     

                self.blocks.clear(self.window, self.background)
                dirty += self.blocks.draw(self.window)
                pygame.display.update(dirty) 
                
        

    def handleEvents(self):
        
        for event in pygame.event.get():          # poll for pygame events
            if event.type == QUIT:
                pygame.quit()
                sys.exit()                

            elif event.type == KEYDOWN:            # handle user input
                if event.key in [pygame.K_ESCAPE, ord('q'), ord('Q'),ord('N'),ord('n')]:
                    pygame.quit()
                    sys.exit()
                elif event.key in [ord('Y'), ord('y')]:
                    game = Game()
                    game.run()   #Play again
                              
                if event.key == K_LEFT:  # paddle control
                    self.paddle.left()
                if event.key == K_RIGHT:
                    self.paddle.right()

                if event.key == K_SPACE:     #serve with space
                    if self.isReset:
                        self.ball.serve()
                        self.isReset = False

            
            elif event.type == KEYUP:      # paddle control
                if event.key == K_LEFT:
                    self.paddle.right()
                if event.key == K_RIGHT:
                    self.paddle.left()
        return True


    def manageBall(self):

        if self.isReset:
            self.ball.rect.centerx = self.paddle.rect.centerx
            return

        if self.ball.rect.top < 800:
            if self.ball.rect.top <= 50:
                self.ball.rect.top = 51
                self.ball.vely *= -1

            if self.ball.rect.left <= 0:
                self.ball.rect.left = 1
                self.ball.velx *= -1

            elif self.ball.rect.right > 800:
                self.ball.rect.right = 799
                self.ball.velx *= -1

        else:
            if self.ball.rect.bottom > 699:  #ball hits bottom = DIE , reset!
                self.reset()


    def manageCollisions(self):
        if pygame.sprite.collide_rect(self.ball, self.paddle):
            self.collisionHelper(self.ball, self.paddle)

        collisions = pygame.sprite.spritecollide(self.ball, self.blocks, dokill=False)

        if len(collisions) >= 2:
            
            if collisions[0].rect.y == collisions[1].rect.y:
                self.ball.vely *= -1
                self.ball.rect.top = collisions[0].rect.bottom + 1

            else:
                
                if self.ball.velx > 0:
                    self.ball.rect.right = collisions[0].rect.left - 1
                else: 
                    self.ball.rect.left = collisions[0].rect.right + 1
                self.ball.velx *= -1

            for block in collisions:
                destroyed, points = block.hit(self.ball.damage)
                self.score.add(points)
                if destroyed:
                    self.blocks.remove(block)

        if len(collisions) == 1:
            item = collisions[0]
            self.collisionHelper(self.ball, item)
            if hasattr(item, 'hit'):
                destroyed, points = item.hit(self.ball.damage)
                self.score.add(points)
                if destroyed:
                    self.blocks.remove(item)

    def collisionHelper(self, ball, item):
        cornerwidth = 5
        if hasattr(item, 'velocity'):
            self.ball.velx += item.velocity/3.0

        if self.ball.rect.colliderect( pygame.Rect(item.rect.left, item.rect.top, cornerwidth, cornerwidth) ):
            speed = math.hypot(self.ball.velx, self.ball.vely)
            component = speed * .7071
            if self.ball.velx >= 0:
                self.ball.velx = -component
            if self.ball.vely >= 0:
                self.ball.vely = -component
            self.ball.rect.bottom = item.rect.top -1
            return

        #ball hit top right
        if self.ball.rect.colliderect( pygame.Rect(item.rect.right, item.rect.top, cornerwidth, cornerwidth) ):
            speed = math.hypot(self.ball.velx, self.ball.vely)
            component = speed * .7071
            if self.ball.velx <= 0:
                self.ball.velx = component
            if self.ball.vely >= 0:
                self.ball.vely = -component
            self.ball.rect.bottom = item.rect.top -1
            return

        #ball hit bottom left
        if self.ball.rect.colliderect( pygame.Rect(item.rect.left, item.rect.bottom-cornerwidth, cornerwidth, cornerwidth) ):
            speed = math.hypot(self.ball.velx, self.ball.vely)
            component = speed * .7071
            if self.ball.velx >= 0:
                self.ball.velx = -component
            if self.ball.vely <= 0:
                self.ball.vely = component
            self.ball.rect.top  = item.rect.bottom + 1
            return

        #ball hit bottom right
        if self.ball.rect.colliderect( pygame.Rect(item.rect.left, item.rect.bottom-cornerwidth, cornerwidth, cornerwidth) ):
            speed = math.hypot(self.ball.velx, self.ball.vely)
            component = speed * .7071
            if self.ball.velx <= 0:
                self.ball.velx = component
            if self.ball.vely <= 0:
                self.ball.vely = component
            self.ball.rect.top = item.rect.bottom + 1
            return

        #ball hit top edge
        if self.ball.rect.colliderect( pygame.Rect(item.rect.left, item.rect.top, item.rect.width, 2) ):
            self.ball.vely *= -1
            self.ball.rect.bottom = item.rect.top - 1
            return

        #ball hit bottom edge
        elif self.ball.rect.colliderect( pygame.Rect(item.rect.left, item.rect.bottom-2, item.rect.width, 2) ):
            self.ball.vely *= -1
            self.ball.rect.top = item.rect.bottom + 1
            return

        #ball hit left side
        if self.ball.rect.collidepoint((item.rect.left, item.rect.centery)):
            self.ball.velx *= -1
            self.ball.rect.right = item.rect.left - 1
            return

        #ball hit right side
        elif self.ball.rect.collidepoint((item.rect.right, item.rect.centery)):
            self.ball.velx *= -1
            self.ball.rect.left = item.rect.right + 1
            return

    def managePowerups(self):
        if self.currentpowerup is None:
            if not self.isReset:
                self.powerupdrop -= 1

                if self.powerupdrop <= 0:
                    droppercentages = [
                    (30, '1up'),
                    (60, 'slowball'),
                    (100, 'bigpaddle')
                    ]

                    choice = random.uniform(0,100)
                    for chance, type in droppercentages:
                        if choice <= chance:
                            self.currentpowerup = Powerup(type)
                            self.sprites.add(self.currentpowerup)
                            break
            return

        if not self.currentpowerup.collected:
            if self.paddle.rect.colliderect(self.currentpowerup.rect):
                if self.currentpowerup.type == 'bigpaddle':
                    self.paddle.grow()

                elif self.currentpowerup.type == '1up':
                    self.lives.setLives( self.lives.getLives() + 1)

                elif self.currentpowerup.type == 'slowball':
                    self.ball.slowDown()

                self.currentpowerup.collected = True
                self.sprites.remove(self.currentpowerup)
                
            else:
                
                alive = self.currentpowerup.update()
                if not alive:
                    self.sprites.remove(self.currentpowerup)
                    self.currentpowerup = None

                    self.powerupdrop = random.randint(60 * 10, 60 * 20)

        elif self.currentpowerup.countdown > 0:
            self.currentpowerup.countdown -= 1
            
        else:
            
            if self.currentpowerup is not None:
                if self.currentpowerup.type == 'bigpaddle':
                    self.paddle.shrink()

                elif self.currentpowerup.type == 'slowball':
                    self.ball.speedUp()

                self.currentpowerup = None
                self.powerupdrop = random.randint(60 * 30, 60 * 60)


    def newLevel(self):
        self.currentlevel += 1
        if os.path.isfile(os.path.join('levels', 'level%d.level'%self.currentlevel)):
            self.loadLevel(self.currentlevel)
            ShowLevel.add(self.level)
            self.sprites.add(self.level)
            self.paddle.reset()
            self.ball.reset()
            self.isReset = True

        else:  #don't have next level
            self.winner = pygame.image.load(os.path.join('images','winner.png'))
            self.win_sound = pygame.mixer.Sound(os.path.join('sounds','TaDa.wav'))
            self.win_sound.play(0)
            self.window.blit(self.winner,(110,100))
            pygame.display.flip()
            
            self.playing = False


    def reset(self):
        lives = self.lives.getLives()
        if lives > 0:
            self.lives.setLives(lives-1)
            self.paddle.reset()
            self.ball.reset()
            self.isReset = True
        else:
            font = pygame.font.Font('KGWakeMeUp.ttf', 72)
            endmessage1 = font.render("G", True, (250,50,139))
            endmessagerect1  = endmessage1.get_rect()
            endmessagerect1.center = (180, 400)
            self.window.blit(endmessage1, endmessagerect1)
            endmessage2 = font.render("A", True, (142,72,229))
            endmessagerect2  = endmessage2.get_rect()
            endmessagerect2.center = (241, 400)
            self.window.blit(endmessage2, endmessagerect2)
            endmessage3 = font.render("M", True, (247,143,32))
            endmessagerect3  = endmessage3.get_rect()
            endmessagerect3.center = (303, 400)
            self.window.blit(endmessage3, endmessagerect3)
            endmessage4 = font.render("E", True, (162,254,81))
            endmessagerect4  = endmessage4.get_rect()
            endmessagerect4.center = (364, 400)
            self.window.blit(endmessage4, endmessagerect4)            
            endmessage5 = font.render("O", True, (44,235,242))
            endmessagerect5  = endmessage5.get_rect()
            endmessagerect5.center = (445, 400)
            self.window.blit(endmessage5, endmessagerect5)
            endmessage6 = font.render("V", True, (49,236,154))
            endmessagerect6  = endmessage6.get_rect()
            endmessagerect6.center = (503, 400)
            self.window.blit(endmessage6, endmessagerect6)
            endmessage7 = font.render("E", True, (244,242,44))
            endmessagerect7  = endmessage7.get_rect()
            endmessagerect7.center = (562, 400)
            self.window.blit(endmessage7, endmessagerect7)
            endmessage8 = font.render("R", True, (242,92,92))
            endmessagerect8  = endmessage8.get_rect()
            endmessagerect8.center = (620, 400)
            self.window.blit(endmessage8, endmessagerect8)
            
            font3 = pygame.font.Font('5012_tLU_infection.ttf', 35)
            endmessage = font3.render("Play again ?  [ Y / N ]", True, (241,34,63))
            endmessagerect  = endmessage.get_rect()
            endmessagerect.center = (405, 470)
            self.window.blit(endmessage, endmessagerect)            
                        
            self.end_sound = pygame.mixer.Sound(os.path.join('sounds','theEnd.wav'))
            self.end_sound.play(2)
            
            pygame.display.flip()
            self.playing = False


    def parseLevelFile(self, filepath):
        defaultlevel = [
            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
            [0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0],
            [3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3],
            [0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0],
            [0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0],
            [5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5],
            [0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]

        if os.path.isfile(filepath):
            f = open(filepath, 'r')
            rows = f.readlines()

            level = []
            for r in rows:
                blocks = r.strip().split(' ')
                newrow = []
                for b in blocks:
                    newrow.append(int(b))
                level.append(newrow)
            return level

        else:
            return defaultlevel

    def loadLevel(self, levelnumber):
        level = self.parseLevelFile( os.path.join('levels', 'level%d.level' % levelnumber))

        for i in range(11):              
            for j in range(15):         
                if level[i][j] != 0:    
                    blocklevel = level[i][j]    
                    x = 60 + (48*j)                
                    y = 75 + (28*i) 
         
            # create a block and add it
                    
                    if blocklevel > 0 and blocklevel < 26:
                        self.blocks.add(self.blockfactory.getBlock((x,y), blocklevel))
        
                    elif blocklevel == 26:
                        self.blocks.add(self.blockfactory.getSolidBlock((x,y)))


if __name__ == '__main__':
    game = Game()    
    game.run()
    